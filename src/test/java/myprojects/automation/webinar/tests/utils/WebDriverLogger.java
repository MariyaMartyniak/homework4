package myprojects.automation.webinar.tests.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.openqa.selenium.support.events.WebDriverEventListener;

import java.util.Arrays;

public class  WebDriverLogger implements WebDriverEventListener {


    @Override
    public void beforeNavigateTo(String s, WebDriver webDriver) {
        System.out.println("The page (url) " + s + " is going to be opened");

    }

    @Override
    public void afterNavigateTo(String s, WebDriver webDriver) {
        System.out.println("The page (url) " + s + " is opened");
    }

    @Override
    public void beforeNavigateBack(WebDriver webDriver) {

    }

    @Override
    public void afterNavigateBack(WebDriver webDriver) {

    }

    @Override
    public void beforeNavigateForward(WebDriver webDriver) {

    }

    @Override
    public void afterNavigateForward(WebDriver webDriver) {

    }


    @Override
    public void beforeNavigateRefresh(WebDriver webDriver) {
        System.out.println("The page is going to be refreshed");

    }

    @Override
    public void afterNavigateRefresh(WebDriver webDriver) {
        System.out.println("The page is refreshed");

    }

    @Override
    public void beforeFindBy(By by, WebElement webElement, WebDriver webDriver) {
        System.out.println("Search for element: " + by.toString());
    }

    @Override
    public void afterFindBy(By by, WebElement webElement, WebDriver webDriver) {
        System.out.println("Element " + by.toString() + " found successfully");
    }

    @Override
    public void beforeClickOn(WebElement webElement, WebDriver webDriver) {
        System.out.println("Click on element: " + webElement.getTagName() + " " + webElement.getAttribute("name"));
    }

    @Override
    public void afterClickOn(WebElement webElement, WebDriver webDriver) {

    }


    @Override
    public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {


        System.out.println("Fill input " + webElement.getAttribute("id") + " with value " + Arrays.asList(charSequences.toString()));
    }

    @Override
    public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        System.out.println("Value " + webElement.getAttribute("id") + " with value " + charSequences.toString() + " is successfully changed");
    }

    @Override
    public void beforeScript(String s, WebDriver webDriver) {
        System.out.println("The " + s + " script is going to be executed");

    }

    @Override
    public void afterScript(String s, WebDriver webDriver) {
        System.out.println("The " + s + " script is be executed");

    }

    @Override
    public void onException(Throwable throwable, WebDriver webDriver) {
        System.out.println("Exception occurs and needs to be handled");
    }
}
